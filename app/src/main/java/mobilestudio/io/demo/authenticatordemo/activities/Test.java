package mobilestudio.io.demo.authenticatordemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import mobilestudio.io.demo.authenticatordemo.R;

public class Test extends AppCompatActivity   {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        findViewById(R.id.ld).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent (Test.this , LoginDefaultActivity.class));
            }
        });
        findViewById(R.id.lc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent (Test.this , LoginCustomActivity.class));
            }
        });
        findViewById(R.id.sd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent (Test.this , SignUpDefaultActivity.class));
            }
        });
        findViewById(R.id.sc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent (Test.this , SignUpCostumActivity.class));
            }
        });
        findViewById(R.id.tpn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent (Test.this , TestphoneNumberAuth.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId() ;
        if(id== R.id.action_logout){
            if (FirebaseAuth.getInstance() != null )
                 FirebaseAuth.getInstance().signOut();

                 Toast.makeText(this, "Logging out Successfully xD", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
