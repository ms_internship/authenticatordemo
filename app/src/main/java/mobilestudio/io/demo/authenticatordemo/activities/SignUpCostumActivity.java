package mobilestudio.io.demo.authenticatordemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import mobilestudio.io.component.authenticator.core.OnAuthListener;
import mobilestudio.io.component.authenticator.core.authenticate.FirebaseAuthenticator;
import mobilestudio.io.component.authenticator.core.builder.SignUpAuthBuilder;
import mobilestudio.io.component.authenticator.core.datahandle.FirebaseDBHandler;
import mobilestudio.io.component.authenticator.core.verification.FirebasePhoneNumberVerifier;
import mobilestudio.io.component.authenticator.ui.model.User;
import mobilestudio.io.demo.authenticatordemo.R;

import static mobilestudio.io.component.authenticator.R.id.bt_signUp;
import static mobilestudio.io.component.authenticator.R.id.ed_birthDatee;
import static mobilestudio.io.component.authenticator.R.id.ed_phone;


public class SignUpCostumActivity extends AppCompatActivity  implements OnAuthListener {
    EditText email , password , confrimPassword , phone , birthdate   ;
    Button signupButton ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_costum);
        email = (EditText) findViewById(R.id.ed_EmailAddress);
        password = (EditText) findViewById(R.id.ed_password );
        confrimPassword = (EditText) findViewById(R.id.ed_confrimPassword);
        signupButton = (Button) findViewById(bt_signUp);
        phone = (EditText) findViewById(ed_phone);
        birthdate = (EditText) findViewById(ed_birthDatee);
        new SignUpAuthBuilder(new FirebaseAuthenticator(),new FirebaseDBHandler("UserProfile")
                ,new FirebasePhoneNumberVerifier(this),this)
                        .enableEmailAndPasswordView(email,password,confrimPassword,signupButton,true)
                        .enablePhoneView(phone,false,true)
                        .enableBirthDateView(birthdate, true , this)
                        .build();
    }

    @Override
    public void onSuccess(User user) {
        startActivity( new Intent(this , MainActivity.class));

    }

    @Override
    public void onFailed(String errorMessage) {
    }
}
