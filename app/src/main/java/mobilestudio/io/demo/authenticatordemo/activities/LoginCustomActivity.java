package mobilestudio.io.demo.authenticatordemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;

import mobilestudio.io.component.authenticator.core.OnAuthListener;
import mobilestudio.io.component.authenticator.core.authenticate.FirebaseAuthenticator;
import mobilestudio.io.component.authenticator.core.builder.LoginAuthBuilder;
import mobilestudio.io.component.authenticator.core.builder.LoginAuthController;
import mobilestudio.io.component.authenticator.core.datahandle.FirebaseDBHandler;
import mobilestudio.io.component.authenticator.ui.model.User;
import mobilestudio.io.demo.authenticatordemo.R;

public class LoginCustomActivity extends AppCompatActivity implements OnAuthListener {
    EditText email , password ,phoneNumber  ;
    Button loginButton , sendCode  ;
    String gmailClientID = "772830892938-d19dp8q1o7brmrg7nlt3sic9u58e2ot8.apps.googleusercontent.com" ;
    SignInButton gmail ;
    LoginButton facebook ;
    LoginAuthController controller ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_custom);
        email = (EditText) findViewById(R.id.ed_EmailAddress);
        password = (EditText) findViewById(R.id.ed_password);
        loginButton = (Button) findViewById(R.id.bt_Login);
        sendCode = (Button) findViewById(R.id.bt_sentVerificationNumber);
        phoneNumber = (EditText) findViewById(R.id.ed_phoneNumber);
        facebook = (LoginButton) findViewById(R.id.login_button );
        gmail = (SignInButton) findViewById(R.id.sign_in_button);
        controller = new LoginAuthBuilder(new FirebaseAuthenticator()
                ,new FirebaseDBHandler("userProfile")
                ,this
                ,this
                )
                .enableCompleteProfile(SignUpDefaultActivity.class)
                .enableEmailAndPassword(email, password, loginButton)
                .enablePhone(phoneNumber,sendCode)
                .enableFacebook(facebook)
                .build();
    }


    @Override
    public void onSuccess(User user) {
        Toast.makeText(this,"I've User now ", Toast.LENGTH_SHORT).show();
        startActivity( new Intent(this , MainActivity.class));
    }

    @Override
    public void onFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controller.onActivityResult(requestCode, resultCode, data);
    }
}
