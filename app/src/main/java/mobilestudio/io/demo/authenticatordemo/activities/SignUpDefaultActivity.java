package mobilestudio.io.demo.authenticatordemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import mobilestudio.io.component.authenticator.core.OnAuthListener;
import mobilestudio.io.component.authenticator.core.authenticate.FirebaseAuthenticator;
import mobilestudio.io.component.authenticator.core.builder.SignUpAuthBuilder;
import mobilestudio.io.component.authenticator.core.builder.SignUpAuthController;
import mobilestudio.io.component.authenticator.core.datahandle.FirebaseDBHandler;
import mobilestudio.io.component.authenticator.core.verification.FirebasePhoneNumberVerifier;
import mobilestudio.io.component.authenticator.ui.model.User;
import mobilestudio.io.demo.authenticatordemo.R;

public class SignUpDefaultActivity extends AppCompatActivity implements OnAuthListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_default);
        User user = new User();
        Bundle bundle;
        if (getIntent().getExtras() != null) {

            bundle = (Bundle) getIntent().getExtras().get("bundle");

            user = (User) bundle.getSerializable("user");

        } else {
            user.setEmail("MustafaPiso@yahoo.com");
            user.setFirstName("Mustafa ");
            user.setLastName("Gamal");
            user.setBirthDate("11/10/1998");
            user.setGender("Male");
        }

        SignUpAuthController controller = new SignUpAuthBuilder(new FirebaseAuthenticator(),
                new FirebaseDBHandler("UsersProfile"),
                new FirebasePhoneNumberVerifier(this), this)
                .defaultView(this, R.id.defaultView)
                .enableDefaultEmailAndPassword(true)
                .enableDefaultBirthDateView(true, this)
                .enableDefaultFirstAndLastName(false)
                .setUserDataAtCompleteProfile(user, getIntent().getBooleanExtra("isSocial", false))
                .build();
    }

    @Override
    public void onSuccess(User user) {
        Toast.makeText(this, " i've user now ", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, MainActivity.class));

    }

    @Override
    public void onFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
