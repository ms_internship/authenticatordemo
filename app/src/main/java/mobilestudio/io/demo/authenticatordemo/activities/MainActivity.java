package mobilestudio.io.demo.authenticatordemo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import mobilestudio.io.demo.authenticatordemo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textView = (TextView) findViewById(R.id.tv_text);
        if(getIntent().getExtras() !=null){
           String uuserName = getIntent().getExtras().getString("user");
            textView.setText(uuserName);
        }

    }

}
