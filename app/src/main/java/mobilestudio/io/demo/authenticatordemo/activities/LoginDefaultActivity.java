package mobilestudio.io.demo.authenticatordemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import mobilestudio.io.component.authenticator.core.OnAuthListener;
import mobilestudio.io.component.authenticator.core.authenticate.FirebaseAuthenticator;
import mobilestudio.io.component.authenticator.core.builder.LoginAuthBuilder;
import mobilestudio.io.component.authenticator.core.builder.LoginAuthController;
import mobilestudio.io.component.authenticator.core.datahandle.FirebaseDBHandler;
import mobilestudio.io.component.authenticator.ui.model.User;
import mobilestudio.io.demo.authenticatordemo.R;

public class LoginDefaultActivity extends AppCompatActivity implements OnAuthListener {
    LoginAuthController controller;
    String gmailClientID = "772830892938-d19dp8q1o7brmrg7nlt3sic9u58e2ot8.apps.googleusercontent.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_default);
        Bundle bundle = new Bundle();

        controller = new LoginAuthBuilder(new FirebaseAuthenticator()
                , new FirebaseDBHandler("UserProfile"), this,
                this)
            //   .enableCompleteProfile( SignUpDefaultActivity.class)
               // .setBundle(bundle)
                .defaultView(R.id.defaultView)
                .enableDefaultEmailAndPassword()
                .enableDefaultGmail(gmailClientID)
                .enableDefaultPhone()
                .enableDefaultFacebook()
                .build();
    }

    @Override
    public void onSuccess(User user) {
        Toast.makeText(this, "I'm getting the user now ", Toast.LENGTH_SHORT).show();
        Log.v("User ", user.getFirstName() + " " + user.getLastName() + " with Id " + user.getUid());
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("user", user.getFirstName() + " " + user.getLastName() + " with Id " + user.getUid());
        startActivity(intent);
    }

    @Override
    public void onFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controller.onActivityResult(requestCode, resultCode, data);
    }
}