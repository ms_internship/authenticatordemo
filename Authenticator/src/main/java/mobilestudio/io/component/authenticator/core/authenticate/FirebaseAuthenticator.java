package mobilestudio.io.component.authenticator.core.authenticate;

import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import mobilestudio.io.component.authenticator.core.login.PhoneNumberLoginCallBack;

/**
 * Created by Sayed on 9/7/2017.
 */

public class FirebaseAuthenticator implements Authenticator {

    private FirebaseAuth firebaseAuth;

    public FirebaseAuthenticator() {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void loginWithPhoneNumber(PhoneAuthCredential credential, final PhoneNumberLoginCallBack loginCallBack) {
        firebaseAuth.signInWithCredential(credential).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                loginCallBack.OnAddSuccessfully();
                //  callback.onSuccess("Authenticated Successfully");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                loginCallBack.onAddWithFailure();
                // callback.onFailed(e.getMessage());
            }
        });
    }

    @Override
    public void loginWithPhoneNumber(PhoneAuthCredential credential, final OnAuthenticateCallback callback) {
        firebaseAuth.signInWithCredential(credential).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                callback.onSuccess(firebaseAuth.getCurrentUser().getUid());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                callback.onFailed(e.getMessage());
            }
        });
    }


    @Override
    public void createUserWithEmailandPassword(String email, String password, final OnAuthenticateCallback callback) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                callback.onSuccess(FirebaseAuth.getInstance().getCurrentUser().getUid());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailed(e.getMessage());
            }
        });
    }


    @Override
    public void loginWithEmailandPassword(String email, String password, final OnAuthenticateCallback callback) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                callback.onSuccess(firebaseAuth.getCurrentUser().getUid());

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onFailed(e.getMessage());
            }
        });

    }

    @Override
    public void loginWithFacebook(AccessToken accessToken, final OnAuthenticateCallback callback) {
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());

        firebaseAuth.signInWithCredential(credential)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        callback.onSuccess(firebaseAuth.getCurrentUser().getUid());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callback.onFailed(e.getMessage());
                    }
                });

    }

    @Override
    public void loginWithGmail(String userID, final OnAuthenticateCallback callback) {
        AuthCredential credential = GoogleAuthProvider.getCredential(userID, null);
        firebaseAuth.signInWithCredential(credential)
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        callback.onSuccess(firebaseAuth.getCurrentUser().getUid());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callback.onFailed(e.getMessage());
                    }
                });
    }


}
