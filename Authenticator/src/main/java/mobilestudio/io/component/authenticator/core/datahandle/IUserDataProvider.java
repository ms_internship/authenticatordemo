package mobilestudio.io.component.authenticator.core.datahandle;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by Sayed on 9/18/2017.
 */

public interface IUserDataProvider {


    void loadUserFromDB(String uid , OnDataFetched dataFetched);

    User getUserLocal();


}
