package mobilestudio.io.component.authenticator.core.signup;

import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;

/**
 * Created by pisoo on 9/8/2017.
 */

public abstract class BaseSignUpAuth {
    Authenticator authenticator;

    public BaseSignUpAuth(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    public abstract void signUp(onSignUpCallback callback);
}
