package mobilestudio.io.component.authenticator.core.builder;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.firebase.auth.AuthCredential;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import mobilestudio.io.component.authenticator.core.AuthenticationManager;
import mobilestudio.io.component.authenticator.core.credentials.FirebaseAuthCredential;
import mobilestudio.io.component.authenticator.core.credentials.IAuthCredential;
import mobilestudio.io.component.authenticator.core.datahandle.FirebaseLinker;
import mobilestudio.io.component.authenticator.core.datahandle.ILinker;
import mobilestudio.io.component.authenticator.core.datahandle.OnLinkCallback;
import mobilestudio.io.component.authenticator.ui.model.User;
import mobilestudio.io.component.authenticator.core.signup.BaseSignUpAuth;
import mobilestudio.io.component.authenticator.core.signup.onSignUpCallback;
import mobilestudio.io.component.authenticator.ui.view.DefaultSignUpView;
import mobilestudio.io.component.authenticator.ui.view.RegistrationViews;

/**
 * Created by pisoo on 9/8/2017.
 */

public class SignUpAuthController {
    private BaseSignUpAuth EmailAndPasswordAuth, phoneNumberAuth;
    private User user;
    private Context context;
    private RegistrationViews registrationViews;
    private DefaultSignUpView signUpView;
    private boolean onSocial;
    private AuthenticationManager manager;
    private ILinker linker = new FirebaseLinker();
    private IAuthCredential<AuthCredential> credential = new FirebaseAuthCredential();


    public SignUpAuthController(SignUpAuthBuilder builder) {
        this.user = builder.getUser();
        this.EmailAndPasswordAuth = builder.getEmailAndPasswordAuth();
        this.phoneNumberAuth = builder.getPhoneNumberAuth();
        this.context = builder.getContext();
        this.registrationViews = builder.getRegistrationViews();
        this.signUpView = builder.getSignUpView();
        this.onSocial = builder.isOnSocial();
        this.manager = builder.getManager();

        setRedAsteriskToRequiredViews();
        registrationViews.signup.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkRequiredFieldsAreFilled()) {
                    handleSignUpButtonClickedCases();
                }
            }
        });

        if (registrationViews.birthdate != null)
            registrationViews.birthdate.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    releaseDatePickerDialog(registrationViews.birthdate.view);
                }
            });
        if (user != null) {
            setUserData();
        }
        if (onSocial) {
            hidePasswordView();
        }
    }

    private void hidePasswordView() {
        if (signUpView != null) {
            signUpView.hidePasswordField();
        } else if (registrationViews.password.view != null) {
            registrationViews.password.view.setVisibility(View.GONE);
            registrationViews.confirmPassword.view.setVisibility(View.GONE);
        }

    }

    private void setUserData() {
        if (registrationViews.firstName.view != null && user.getFirstName() != null) {
            registrationViews.firstName.view.setText(user.getFirstName());
        }
        if (registrationViews.lastName.view != null && user.getLastName() != null) {
            registrationViews.lastName.view.setText(user.getLastName());
        }
        if (registrationViews.email.view != null && user.getEmail() != null) {
            registrationViews.email.view.setText(user.getEmail());
        }
        if (registrationViews.phone.view != null && user.getPhone() != null) {
            registrationViews.phone.view.setText(user.getPhone());
        }
        if (registrationViews.birthdate.view != null && user.getBirthDate() != null) {
            registrationViews.birthdate.view.setText(user.getBirthDate());
        }
        if (registrationViews.gender.view != null && user.getGender() != null) {
            if (user.getGender().toLowerCase().equals("male")) {
                registrationViews.gender.view.setSelection(1, true);
            } else if (user.getGender().toLowerCase().equals("female")) {
                registrationViews.gender.view.setSelection(2, true);
            }
        }
    }

    private void setRedAsteriskToRequiredViews() {
        if (registrationViews.firstName.view != null && registrationViews.firstName.isRequired) {
            registrationViews.firstName.view.setHint(getRedAstrick(registrationViews.firstName.view.getHint().toString()));
        }
        if (registrationViews.lastName.view != null && registrationViews.lastName.isRequired) {
            registrationViews.lastName.view.setHint(getRedAstrick(registrationViews.lastName.view.getHint().toString()));
        }
        if (registrationViews.email.view != null && registrationViews.email.isRequired) {
            registrationViews.email.view.setHint(getRedAstrick(""));
        }
        if (registrationViews.password.view != null && registrationViews.password.isRequired) {
            registrationViews.password.view.setHint(getRedAstrick(""));
        }
        if (registrationViews.confirmPassword.view != null && registrationViews.confirmPassword.isRequired) {
            registrationViews.confirmPassword.view.setHint(getRedAstrick(""));
        }
        if (registrationViews.phone.view != null && registrationViews.phone.isRequired) {
            registrationViews.phone.view.setHint(getRedAstrick(""));
        }
        if (registrationViews.birthdate.view != null && registrationViews.birthdate.isRequired) {
            registrationViews.birthdate.view.setHint(getRedAstrick(""));
        }
    }

    private SpannableStringBuilder getRedAstrick(String hint) {
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();

        builder.append(hint);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return builder;
    }

    private void releaseDatePickerDialog(final EditText mBirthDate) {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, dayOfMonth);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM/dd/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                mBirthDate.setText(sdf.format(myCalendar.getTime()));
            }
        }, 1988, 1, 1);
        dialog.show();
        mBirthDate.setTransformationMethod(null);
    }

    private boolean checkRequiredFieldsAreFilled() {
        boolean isFilled = true;
        if (!isFilled(registrationViews.firstName.view) && isVisible(registrationViews.firstName.view) && registrationViews.firstName.isRequired) {
            isFilled = false;
            registrationViews.firstName.view.setError("This field is required");
        }
        if (!isFilled(registrationViews.lastName.view) && isVisible(registrationViews.lastName.view) && registrationViews.lastName.isRequired) {
            isFilled = false;
            registrationViews.lastName.view.setError("This field is required");
        }
        if (!isFilled(registrationViews.email.view) && isVisible(registrationViews.email.view) && registrationViews.email.isRequired) {
            isFilled = false;
            registrationViews.email.view.setError("This field is required");
        }
        if (!isFilled(registrationViews.password.view) && isVisible(registrationViews.password.view) && registrationViews.password.isRequired) {
            isFilled = false;
            registrationViews.password.view.setError("This field is required");
        }
        if (!isFilled(registrationViews.confirmPassword.view) && isVisible(registrationViews.confirmPassword.view) && registrationViews.confirmPassword.isRequired) {
            isFilled = false;
            registrationViews.confirmPassword.view.setError("This field is required");
        }
        if (!isFilled(registrationViews.phone.view) && isVisible(registrationViews.phone.view) && registrationViews.phone.isRequired) {
            isFilled = false;
            registrationViews.phone.view.setError("This field is required");
        }
        if (!isFilled(registrationViews.birthdate.view) && isVisible(registrationViews.birthdate.view) && registrationViews.birthdate.isRequired) {
            isFilled = false;
            registrationViews.birthdate.view.setError("This field is required");
        }
        /*
            check if checkbox is Filled or not

          */
        return isFilled;
    }

    private boolean isVisible(EditText view) {

        if (view.isShown())
            return true;
        return false;
    }

    private boolean isFilled(EditText view) {
        if ((view != null && view.getText().toString().isEmpty())) {
            return false;
        }

        return true;
    }

    private void handleSignUpButtonClickedCases() {
        if (onSocial) {
            manager.onSuccessNormal(user.getUid());
        }
        if (!onSocial) {
            if (phoneNumberAuth != null && EmailAndPasswordAuth != null) {
                phoneNumberAuth.signUp(new onSignUpCallback() {
                    @Override
                    public void onSuccess() {
                        String email = registrationViews.email.view.getText().toString();
                        String password = registrationViews.password.view.getText().toString();
                        linker.link(credential.getEmailAndPasswordAuthCredential(email, password), new OnLinkCallback() {
                                    @Override
                                    public void onSuccess(String uid) {
                                        Log.v("UID", uid);
                                    }

                                    @Override
                                    public void onFailed(String errorMessage) {
                                        Log.v("UIDError", errorMessage);
                                    }
                                }
                        );
                    }
                });
            } else if (phoneNumberAuth != null) {
                phoneNumberAuth.signUp(null);
            } else if (EmailAndPasswordAuth != null) {
                EmailAndPasswordAuth.signUp(null);
            }
        }
    }
}
