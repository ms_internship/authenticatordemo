package mobilestudio.io.component.authenticator.core.datahandle;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by Sayed on 9/12/2017.
 */

public interface IDataHandle {

    User getUserData() throws InterruptedException;
}
