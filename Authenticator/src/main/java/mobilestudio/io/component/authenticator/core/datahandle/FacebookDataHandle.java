package mobilestudio.io.component.authenticator.core.datahandle;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONObject;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by Sayed on 9/13/2017.
 */

public class FacebookDataHandle implements IDataHandle {
    private AccessToken accessToken;
    private User returnedUser;

    public FacebookDataHandle(AccessToken accessToken) {
        this.accessToken = accessToken;
        returnedUser = new User();
    }

    @Override
    public User getUserData() throws InterruptedException {
        GraphRequest.Callback callback = new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                try {

                    returnedUser.setEmail(jsonObject.getString("email"));
                    returnedUser.setFirstName(jsonObject.getString("name"));
                    returnedUser.setGender(jsonObject.getString("gender"));
                    returnedUser.setBirthDate(jsonObject.getString("birthday")); // 01/31/1980 format

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        final GraphRequest graphRequest = new GraphRequest(accessToken, "/" + accessToken.getUserId() , null, HttpMethod.GET, callback);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday");
        graphRequest.setParameters(parameters);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                GraphResponse gResponse = graphRequest.executeAndWait();
            }
        });
        t.start();
        t.join();
        return returnedUser;
    }

}
