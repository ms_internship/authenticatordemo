package mobilestudio.io.component.authenticator.ui.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import mobilestudio.io.component.authenticator.R;

/**
 * Created by pisoo on 9/7/2017.
 */

public class DefaultLoginView extends LinearLayout {
    EditText email;
    EditText password , phoneNumber;
    Button emailLoginButton;
    Button facebookLoginButton;
    Button gmailLoginButton;
    Button sentVerificationButton ;

    TextInputLayout emailEdit, passwordEdit;
    LinearLayout emailLayout , phoneNumberLayer ;

    public DefaultLoginView(Context context) {
        super(context);
        inflate(context);
    }

    public DefaultLoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context);
    }

    public DefaultLoginView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DefaultLoginView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context);
    }

    public EditText getPhoneNumber() {
        return phoneNumber;
    }

    public Button getSentVerificationButton() {
        return sentVerificationButton;
    }

    private void inflate(Context context) {
        inflate(context, R.layout.default_login, this);
        email = (EditText) findViewById(R.id.ed_EmailAddress);
        password = (EditText) findViewById(R.id.ed_password);
        emailLoginButton = (Button) findViewById(R.id.emaillogin);
        facebookLoginButton = findViewById(R.id.facebooklogin);
        gmailLoginButton = findViewById(R.id.gmaillogin);
        emailLayout = findViewById(R.id.email_password_layout);
        sentVerificationButton = findViewById(R.id.bt_sentVerificationNumber);
        phoneNumber = findViewById(R.id.ed_phoneNumber);
        phoneNumberLayer = findViewById(R.id.ll_PhoneNumberLayer);
        disableEmail();
        disableFacebook();
        disablePhoneNumber();
        disableGmail();
    }

    private void disablePhoneNumber() {
        phoneNumberLayer.setVisibility(GONE);
    }

    public void disableEmail() {

        emailLayout.setVisibility(View.GONE);
    }

    public void disableFacebook() {
        facebookLoginButton.setVisibility(View.GONE);
    }

    public void disableGmail() {
        gmailLoginButton.setVisibility(View.GONE);
    }

    public EditText getEmail() {
        return email;
    }

    public EditText getPassword() {
        return password;
    }

    public Button getEmailLoginButton() {
        return emailLoginButton;
    }

    public Button getFacebookLoginButton() {
        return facebookLoginButton;
    }

    public Button getGmaiLoginButton() {
        return gmailLoginButton;
    }

    public void enableFacebook() {
        facebookLoginButton.setVisibility(View.VISIBLE);
    }

    public void enableGmail() {
        gmailLoginButton.setVisibility(View.VISIBLE);
    }

    public void enableEmail() {
        emailLayout.setVisibility(View.VISIBLE);
    }
    public void enablePhoneNumber() {
        phoneNumberLayer.setVisibility(View.VISIBLE);
    }

}
