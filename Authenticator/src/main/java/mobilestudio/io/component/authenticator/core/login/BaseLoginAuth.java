package mobilestudio.io.component.authenticator.core.login;

import android.app.Activity;

import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;

/**
 * Created by Sayed on 9/7/2017.
 */

public abstract class BaseLoginAuth {
    Authenticator authenticator;
    public Activity activity;

    public BaseLoginAuth(Authenticator authenticator, Activity activity) {
        this.authenticator = authenticator;
        this.activity = activity;

    }

    public Activity getActivity() {
        return activity;
    }

    public abstract void login();
}
