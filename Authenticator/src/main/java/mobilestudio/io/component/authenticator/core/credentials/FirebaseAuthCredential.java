package mobilestudio.io.component.authenticator.core.credentials;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthProvider;

/**
 * Created by pisoo on 9/22/2017.
 */

public class FirebaseAuthCredential implements IAuthCredential<AuthCredential> {
    @Override
    public AuthCredential getGmailAuthCredential(String googleIdToken) {
        return GoogleAuthProvider.getCredential(googleIdToken, null);
    }

    @Override
    public AuthCredential getFacebookAuthCredential(String token) {
        return FacebookAuthProvider.getCredential(token);
    }

    @Override
    public AuthCredential getEmailAndPasswordAuthCredential(String email, String password) {
        return EmailAuthProvider.getCredential(email, password);
    }

    @Override
    public AuthCredential getPhoneNumberAuthCredential(String verificationId, String code) {
        return PhoneAuthProvider.getCredential(verificationId, code);
    }
}
