package mobilestudio.io.component.authenticator.core.login;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;

import mobilestudio.io.component.authenticator.core.AuthenticationManager;
import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;
import mobilestudio.io.component.authenticator.core.authenticate.OnAuthenticateCallback;
import mobilestudio.io.component.authenticator.core.datahandle.FacebookDataHandle;
import mobilestudio.io.component.authenticator.core.datahandle.Navigator;

/**
 * Created by Sayed on 9/6/2017.
 */

public class FacebookLoginAuth extends LoginAuthCallback  {
    private CallbackManager callbackManager;
    private Button loginButton;
     private AuthenticationManager manager;

    public FacebookLoginAuth(final Authenticator authenticator , final AuthenticationManager manager, final Navigator firstTimeHandler, final Button loginButton, Activity activity) {
        super(authenticator, activity, firstTimeHandler);

        this.loginButton = loginButton;
        this.manager = manager;

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(
                callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        manager.setNavigator(firstTimeHandler);
                        manager.setDataHandle( new FacebookDataHandle(loginResult.getAccessToken()));
                        authenticator.loginWithFacebook(loginResult.getAccessToken(), new OnAuthenticateCallback() {
                            @Override
                            public void onSuccess(String UID) {
                                manager.onSocialSuccess(UID);
                            }

                            @Override
                            public void onFailed(String errorMessage) {
                                manager.onFail(errorMessage);
                            }
                        });

                    }
                    @Override
                    public void onCancel() {

                        Log.v("FBmessage ", "cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.v("FBmessage ", exception.getMessage());
                    }
                }
        );

        loginButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }


    @Override
    public void login() {

        LoginManager.getInstance().logInWithReadPermissions(
                getActivity(),
                Arrays.asList("user_photos", "email", "user_birthday", "public_profile")
        );

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
