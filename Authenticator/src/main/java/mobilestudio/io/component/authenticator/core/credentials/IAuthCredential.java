package mobilestudio.io.component.authenticator.core.credentials;

/**
 * Created by pisoo on 9/22/2017.
 */

public interface IAuthCredential<T> {
    T getGmailAuthCredential(String googleIdToken);
    T getFacebookAuthCredential(String token );
    T getEmailAndPasswordAuthCredential(String email , String password );
    T getPhoneNumberAuthCredential(String verificationId, String code);
}
