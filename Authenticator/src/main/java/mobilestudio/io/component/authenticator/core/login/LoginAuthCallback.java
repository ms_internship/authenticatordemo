package mobilestudio.io.component.authenticator.core.login;

import android.app.Activity;
import android.content.Intent;

import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;
import mobilestudio.io.component.authenticator.core.datahandle.Navigator;

/**
 * Created by Sayed on 9/7/2017.
 */

public abstract class LoginAuthCallback extends BaseLoginAuth {
    public Navigator navigator;

    public LoginAuthCallback(Authenticator authenticator, Activity activity , Navigator firstTimeHandler) {
        super(authenticator, activity);
        this.navigator = firstTimeHandler;
    }

   public abstract void onActivityResult(int requestCode, int resultCode, Intent data) ;
}
