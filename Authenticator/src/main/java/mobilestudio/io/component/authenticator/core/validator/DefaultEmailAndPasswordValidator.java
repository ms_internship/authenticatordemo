package mobilestudio.io.component.authenticator.core.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pisoo on 9/7/2017.
 */

public class DefaultEmailAndPasswordValidator implements EmailAndPasswordValdiator {
    private static final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    @Override
    public Boolean validatePassword(String password) {
        return password.length() > 5 ;
    }

    @Override
    public Boolean validateEmail(String email) {
        Matcher matcher;
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
