package mobilestudio.io.component.authenticator.core.authenticate;

import com.facebook.AccessToken;
import com.google.firebase.auth.PhoneAuthCredential;

import java.io.Serializable;

import mobilestudio.io.component.authenticator.core.login.PhoneNumberLoginCallBack;

/**
 * Created by Sayed on 9/6/2017.
 */

public interface Authenticator extends Serializable {

    void createUserWithEmailandPassword(String email, String password, OnAuthenticateCallback onAuthenticateCallback);

    void loginWithPhoneNumber(PhoneAuthCredential credential, PhoneNumberLoginCallBack loginCallBack);

    void loginWithPhoneNumber(PhoneAuthCredential credential , OnAuthenticateCallback onAuthenticateCallback);

    void loginWithEmailandPassword(String email, String password , OnAuthenticateCallback onAuthenticateCallback);

    void loginWithFacebook(AccessToken accessToken , OnAuthenticateCallback onAuthenticateCallback);

    void loginWithGmail(String userID , OnAuthenticateCallback onAuthenticateCallback);

}
