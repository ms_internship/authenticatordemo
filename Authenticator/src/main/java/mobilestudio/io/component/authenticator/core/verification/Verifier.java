package mobilestudio.io.component.authenticator.core.verification;

import com.google.firebase.auth.PhoneAuthCredential;

/**
 * Created by pisoo on 9/19/2017.
 */

public interface Verifier {
    void sendCode(PhoneAuthCredential credential);

    void resendCode();

    void verify(String phone, OnVerificationCallback callback);
}
