package mobilestudio.io.component.authenticator.core.datahandle;

/**
 * Created by Sayed on 9/18/2017.
 */

public interface OnLinkCallback {

    void onSuccess(String uid);

    void onFailed(String errorMessage);
}
