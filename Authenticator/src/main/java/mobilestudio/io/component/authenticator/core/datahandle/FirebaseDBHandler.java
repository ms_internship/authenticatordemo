package mobilestudio.io.component.authenticator.core.datahandle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by Sayed on 9/18/2017.
 */

public class FirebaseDBHandler implements IDBHandler {
    User retUser;
    DatabaseReference ref;

    public FirebaseDBHandler(String profileNode) {
        retUser = new User();
        ref = FirebaseDatabase.getInstance().getReference(profileNode);

    }

    @Override
    public User getUser(final String uid , final OnDataFetched dataFetched) {
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.child(uid).getValue(User.class);
                if(user != null)
                dataFetched.onSuccess(user);
                else dataFetched.onSuccess(new User());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return retUser;
    }

    @Override
    public void setUser(User user) {
        ref.child(user.getUid())
                .setValue(user);
    }

    @Override
    public void checkFirstTime(final String userID, final FirstTimeCallback callback) {
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    callback.isFirstTime(!dataSnapshot.hasChild(userID));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
