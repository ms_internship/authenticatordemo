package mobilestudio.io.component.authenticator.core.login;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import mobilestudio.io.component.authenticator.core.AuthenticationManager;
import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;
import mobilestudio.io.component.authenticator.core.authenticate.OnAuthenticateCallback;
import mobilestudio.io.component.authenticator.core.validator.DefaultEmailAndPasswordValidator;
import mobilestudio.io.component.authenticator.core.validator.EmailAndPasswordValdiator;


/**
 * Created by Sayed on 9/7/2017.
 */

public class EmailAndPasswordLoginAuth extends LoginAuthCallback {
    EditText email, password;
    EmailAndPasswordValdiator valdiator = new DefaultEmailAndPasswordValidator();
    AuthenticationManager man ;
    public EmailAndPasswordLoginAuth(Authenticator authenticator , AuthenticationManager man, EditText email, EditText password, Button button, Activity activity) {
        super(authenticator, activity, null);
        this.email = email;
        this.password = password;
        this.man = man ;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    @Override
    public void login() {
        String emailString = email.getText().toString();
        String passwordString = password.getText().toString();
        if (valdiator.validateEmail(emailString) && valdiator.validatePassword(passwordString)) {
            authenticator.loginWithEmailandPassword(emailString, passwordString, new OnAuthenticateCallback() {
                @Override
                public void onSuccess(String UID) {
                    man.onSocialSuccess(UID);
                }
                @Override
                public void onFailed(String errorMessage) {
                    man.onFail(errorMessage);
                }
            });

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}
