package mobilestudio.io.component.authenticator.core.datahandle;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Sayed on 9/18/2017.
 */

public class FirebaseLinker implements ILinker {

    private FirebaseAuth firebaseAuth;

    public FirebaseLinker(){
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void link(AuthCredential credential,final OnLinkCallback callback) {
        firebaseAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            callback.onSuccess(task.getResult().getUser().getUid()); // ???
                        } else {
                            callback.onFailed(task.getException().getMessage()); //  ???
                        }
                    }
                });
    }
}
