package mobilestudio.io.component.authenticator.ui.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import mobilestudio.io.component.authenticator.R;

/**
 * Created by pisoo on 9/7/2017.
 */

public class DefaultSignUpView extends LinearLayout {

    Context context;
    EditText firstName, lastName, email, password, confrimassword, phone, birthdate;
    TextInputLayout LfirstName, LlastName, Lemail, Lpassword, Lconfrimassword, Lphone, Lbirthdate;
    LinearLayout nameLayer;
    Button signUp;
    Spinner gender ;


    public DefaultSignUpView(Context context) {
        super(context);
    }

    public DefaultSignUpView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        inflate(context);
    }

    public DefaultSignUpView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DefaultSignUpView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

    }

    private void inflate(final Context context) {
        inflate(context, R.layout.default_signup, this);
        email = findViewById(R.id.ed_EmailAddress);
        password = findViewById(R.id.ed_password);
        confrimassword = findViewById(R.id.ed_confrimPassword);
        signUp = findViewById(R.id.bt_signUp);
        phone = findViewById(R.id.ed_phone);
        birthdate = findViewById(R.id.ed_birthDatee);
        firstName = findViewById(R.id.ed_FirstName);
        lastName = findViewById(R.id.ed_LastName);
        nameLayer = findViewById(R.id.ll_NameLayer);
        gender = findViewById(R.id.sp_Gender);
        hideSignUpView();
    }

    public void setFirstNameVisible() {
        nameLayer.setVisibility(VISIBLE);
        LfirstName.setVisibility(VISIBLE);
    }

    public void setLastNameVisible() {
        LlastName.setVisibility(VISIBLE);
    }

    public void setEmailVisible() {
        Lemail.setVisibility(VISIBLE);
    }

    public void setPasswordVisible() {
        Lpassword.setVisibility(VISIBLE);
    }

    public void setConfrimasswordVisible() {
        Lconfrimassword.setVisibility(VISIBLE);
    }

    public void setPhoneVisible() {
        Lphone.setVisibility(VISIBLE);
    }

    public void setBirthdateVisible() {
        Lbirthdate.setVisibility(VISIBLE);
    }

    public void setSignUVisible() {
        this.signUp.setVisibility(VISIBLE);
    }

    public void setGenderVisible(){this.gender.setVisibility(VISIBLE);}

    private void hideSignUpView() {
        LfirstName = findViewById(R.id.til_firstname);
        LfirstName.setVisibility(GONE);
        LlastName = findViewById(R.id.til_lastName);
        LlastName.setVisibility(GONE);
        Lemail = findViewById(R.id.til_email);
        Lemail.setVisibility(GONE);
        Lpassword = findViewById(R.id.til_password);
        Lpassword.setVisibility(GONE);
        Lconfrimassword = findViewById(R.id.til_confirmPassword);
        Lconfrimassword.setVisibility(GONE);
        Lphone = findViewById(R.id.til_phone);
        Lphone.setVisibility(GONE);
        Lbirthdate = findViewById(R.id.til_birthdate);
        Lbirthdate.setVisibility(GONE);
        nameLayer.setVisibility(GONE);
        gender.setVisibility(GONE);
    }
   public void hidePasswordField(){
       Lpassword.setVisibility(GONE);
       Lconfrimassword.setVisibility(GONE);
   }

    public EditText getEmail() {
        return findViewById(R.id.ed_EmailAddress);
    }

    public EditText getPassword() {
        return password;
    }

    public EditText getConfirmPassword() {
        return confrimassword;
    }

    public Button getSignup() {
        return signUp;
    }

    public EditText getPhone() {
        return phone;
    }

    public EditText getBirthdate() {
        return birthdate;
    }

    public EditText getFirstName() {
        return firstName;
    }

    public EditText getLastName() {
        return lastName;
    }

    public Spinner getGender() {
        return gender;
    }
}
