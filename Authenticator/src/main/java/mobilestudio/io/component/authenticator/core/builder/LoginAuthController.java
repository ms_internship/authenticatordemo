package mobilestudio.io.component.authenticator.core.builder;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.PhoneAuthCredential;

import mobilestudio.io.component.authenticator.core.verification.FirebasePhoneNumberVerifier;
import mobilestudio.io.component.authenticator.core.verification.OnVerificationCallback;
import mobilestudio.io.component.authenticator.core.verification.Verifier;

/**
 * Created by pisoo on 9/8/2017.
 */

public class LoginAuthController {

    private LoginAuthBuilder builder;
    private Activity activity ;

    public LoginAuthController(LoginAuthBuilder builder) {
        this.builder = builder;
        this.activity =builder.getActivity();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        builder.onActivityResult(requestCode, resultCode, data);
    }
    public void verifyPhoneNumber (String phoneNumber ){
        Verifier verifier = new FirebasePhoneNumberVerifier(activity) ;
        verifier.verify(phoneNumber, new OnVerificationCallback() {
            @Override
            public void onSuccess(PhoneAuthCredential credential) {
                Log.v("Name", "success");
            }

            @Override
            public void onFailed(String message) {
                Log.v("Name", message);
            }
        });
    }
}
