package mobilestudio.io.component.authenticator.core.datahandle;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by Sayed on 9/18/2017.
 */

public interface IDBHandler {

    User getUser(String uid , OnDataFetched dataFetched);

    void setUser(User user );

    void checkFirstTime(String userID , FirstTimeCallback callback);

}
