package mobilestudio.io.component.authenticator.core.verification;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by pisoo on 9/19/2017.
 */

public class FirebasePhoneNumberVerifier implements Verifier, Serializable {

    private String phone;
    private Activity activity;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mToken = null;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private VerificationNumberDialog dialog;
    private boolean dialogIsPopedUp = false;
    private OnVerificationCallback callback;
    private ProgressDialog progress;


    public FirebasePhoneNumberVerifier(Activity activity) {
        this.activity = activity;
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(final PhoneAuthCredential phoneAuthCredential) {
                callback.onSuccess(phoneAuthCredential);
                dismissDialog();
            }
            @Override
            public void onVerificationFailed(FirebaseException e) {
                callback.onFailed(e.getMessage());
                Log.v("error ", e.getMessage());
            }
            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(verificationId, forceResendingToken);
                mToken = forceResendingToken;
                mVerificationId = verificationId;
                if (dialog == null ) {
                    showDialog();
                }else if (!dialog.isVisible()){
                    showDialog();
                }
            }
        };
    }

    public void verify(String phone, OnVerificationCallback callback) {
        this.phone = phone;
        this.callback = callback;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                activity, mCallbacks,mToken);
    }
    @Override
    public void resendCode() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                activity, mCallbacks, mToken);
    }

    @Override
    public void sendCode(PhoneAuthCredential credential) {

        callback.onSuccess(credential);
    }

    private void showDialog() {

        dialogIsPopedUp = true;
        Bundle bundle = new Bundle();
        bundle.putString("VerificationId", mVerificationId);
        bundle.putString("phoneNumber", phone);
        bundle.putSerializable("verifier", FirebasePhoneNumberVerifier.this);
        android.app.FragmentManager manager = activity.getFragmentManager();
        dialog = new VerificationNumberDialog();
        dialog.setArguments(bundle);
        dialog.show(manager, null);
    }

    private void dismissDialog() {
        dialogIsPopedUp = false ;
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void initProgressDialog (){
        progress=new ProgressDialog(activity);
        progress.setMessage("Downloading Music");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setProgress(0);

    }
}
