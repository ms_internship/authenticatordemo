package mobilestudio.io.component.authenticator.core.builder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import mobilestudio.io.component.authenticator.core.AuthenticationManager;
import mobilestudio.io.component.authenticator.core.OnAuthListener;
import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;
import mobilestudio.io.component.authenticator.core.datahandle.IDBHandler;
import mobilestudio.io.component.authenticator.core.datahandle.UserDataProvider;
import mobilestudio.io.component.authenticator.core.login.PhoneNumberLoginAuth;
import mobilestudio.io.component.authenticator.core.verification.FirebasePhoneNumberVerifier;
import mobilestudio.io.component.authenticator.ui.view.DefaultLoginView;
import mobilestudio.io.component.authenticator.core.datahandle.Navigator;
import mobilestudio.io.component.authenticator.core.login.LoginAuthCallback;
import mobilestudio.io.component.authenticator.core.login.EmailAndPasswordLoginAuth;
import mobilestudio.io.component.authenticator.core.login.FacebookLoginAuth;
import mobilestudio.io.component.authenticator.core.login.GmailLoginAuth;


public class LoginAuthBuilder {

    private List<LoginAuthCallback> authenticators = new ArrayList<>();
    private Authenticator authenticator;
    private DefaultLoginView loginView;
    private Activity loginActivity;
    private Navigator navigator;

    public Activity getActivity() {
        return loginActivity;
    }

    private AuthenticationManager manager;
    private Class<? extends AppCompatActivity> registerActivity;


    public LoginAuthBuilder(Authenticator authenticator, IDBHandler dbHandler, OnAuthListener authListener, Activity login) {
        this.loginActivity = login;
        this.authenticator = authenticator;
        manager = AuthenticationManager.getInstance(UserDataProvider.getInstance(dbHandler), dbHandler);
        manager.setListener(authListener);
        navigator = new Navigator(loginActivity, registerActivity);

    }

    public LoginAuthBuilder enableCompleteProfile(Class<? extends AppCompatActivity> registerActivity) {
        this.registerActivity = registerActivity;
        return this;
    }

    public LoginAuthBuilder setBundle(Bundle bundle) {
        if (navigator != null)
        navigator.setBundle(bundle);
        return this;
    }

    public LoginAuthBuilder enableEmailAndPassword(EditText email, EditText password, Button login) {
        authenticators.add(new EmailAndPasswordLoginAuth(authenticator, manager, email, password, login, loginActivity));
        return this;
    }

    public LoginAuthBuilder enableFacebook(Button login) {
        authenticators.add(new FacebookLoginAuth(authenticator, manager, navigator, login, loginActivity));
        return this;
    }

    public LoginAuthBuilder enableGmail(Button login, String gmailClientID) {
        authenticators.add(new GmailLoginAuth(authenticator, manager, navigator, login, gmailClientID, loginActivity));
        return this;
    }

    public LoginAuthBuilder enablePhone(EditText number, Button sendCode) {
        authenticators.add(new PhoneNumberLoginAuth(authenticator, manager, new FirebasePhoneNumberVerifier(loginActivity), navigator, loginActivity, number, sendCode));
        return this;
    }

    public LoginAuthBuilder defaultView(int id) {
        loginView = loginActivity.findViewById(id);
        return this;
    }

    public LoginAuthBuilder enableDefaultEmailAndPassword() {
        loginView.enableEmail();
        return enableEmailAndPassword(loginView.getEmail(), loginView.getPassword(), loginView.getEmailLoginButton());
    }

    public LoginAuthBuilder enableDefaultFacebook() {
        loginView.enableFacebook();
        return enableFacebook(loginView.getFacebookLoginButton());
    }

    public LoginAuthBuilder enableDefaultGmail(String gmailClientID) {
        loginView.enableGmail();
        return enableGmail(loginView.getGmaiLoginButton(), gmailClientID);
    }

    public LoginAuthBuilder enableDefaultPhone() {
        loginView.enablePhoneNumber();
        return enablePhone(loginView.getPhoneNumber(), loginView.getSentVerificationButton());
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (LoginAuthCallback loginAuthCallback : authenticators) {
            loginAuthCallback.onActivityResult(requestCode, resultCode, data);
        }
    }

    public LoginAuthController build() {
        manager.setOnSocial(registerActivity != null);
        return new LoginAuthController(this);
    }
}
