package mobilestudio.io.component.authenticator.core.datahandle;

import mobilestudio.io.component.authenticator.ui.model.User;
import mobilestudio.io.component.authenticator.ui.view.RegistrationViews;

/**
 * Created by Sayed on 9/17/2017.
 */

public class UserDataProvider implements IUserDataProvider {
    public void setRegistrationViews(RegistrationViews registrationViews) {
        this.registrationViews = registrationViews;
    }

    private RegistrationViews registrationViews;
    private IDBHandler handler;

    private static UserDataProvider dataProvider;

    public static UserDataProvider getInstance(IDBHandler handler) {
        if (dataProvider == null) {
            dataProvider = new UserDataProvider(handler);
        }
        return dataProvider;
    }

    private UserDataProvider(IDBHandler handler) {
        this.handler = handler;
    }


    @Override
    public User getUserLocal() {
        User user = new User();
        if (registrationViews.firstName.view  != null) {
            user.setFirstName(registrationViews.firstName.view.getText().toString());
        }

        if (registrationViews.lastName.view != null) {
            user.setLastName(registrationViews.lastName.view.getText().toString());
        }

        if (registrationViews.email.view  != null) {
            user.setEmail(registrationViews.email.view.getText().toString());
        }

        if (registrationViews.phone.view != null) {
            user.setPhone(registrationViews.phone.view.getText().toString());
        }
        if (registrationViews.birthdate.view  != null) {
            user.setBirthDate(registrationViews.birthdate.view.getText().toString());
        }
        if (registrationViews.gender.view != null) {
            user.setGender(registrationViews.gender.view.getSelectedItem().toString());
        }
        return user;
    }

    @Override
    public void loadUserFromDB(String uid, final OnDataFetched dataFetched) {
        handler.getUser(uid, new OnDataFetched() {
            @Override
            public void onSuccess(User user) {
                dataFetched.onSuccess(user);
            }
        });

    }


}
