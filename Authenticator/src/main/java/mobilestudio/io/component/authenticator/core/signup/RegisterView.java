package mobilestudio.io.component.authenticator.core.signup;

/**
 * Created by pisoo on 9/13/2017.
 */

public class RegisterView <T> {
    public T view ;
    public boolean isRequired ;
    public void setView(T view , boolean isRequired ){
        this.view = view ;
        this.isRequired = isRequired ;
    }
}
