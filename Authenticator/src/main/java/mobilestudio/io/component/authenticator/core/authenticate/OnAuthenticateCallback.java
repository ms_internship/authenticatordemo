package mobilestudio.io.component.authenticator.core.authenticate;

/**
 * Created by Sayed on 9/6/2017.
 */

public interface OnAuthenticateCallback {

    void onSuccess(String UID);

    void onFailed(String errorMessage);

}
