package mobilestudio.io.component.authenticator.core.login;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import mobilestudio.io.component.authenticator.core.AuthenticationManager;
import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;
import mobilestudio.io.component.authenticator.core.authenticate.OnAuthenticateCallback;
import mobilestudio.io.component.authenticator.core.datahandle.Navigator;
import mobilestudio.io.component.authenticator.core.datahandle.GmailDataHandle;

/**
 * Created by Sayed on 9/7/2017.
 */

public class GmailLoginAuth extends LoginAuthCallback implements GoogleApiClient.OnConnectionFailedListener {
    GoogleApiClient googleApiClient;
    GoogleSignInOptions gso;
    Navigator navigator;
    private static final int RC_SIGN_IN = 1;
    AuthenticationManager manager;

    public GmailLoginAuth(Authenticator authenticator, AuthenticationManager manager, Navigator navigator, Button gmailSignInButton, String clientID, Activity activity) {
        super(authenticator, activity, null);
        this.navigator = navigator;
        this.manager = manager;
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(clientID)
                .build();
        googleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)

                .build();


        gmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

    }

    @Override
    public void login() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("SS", "handleSignInResult:" + result.isSuccess());

        if (result.isSuccess()) {
            final GoogleSignInAccount acct = result.getSignInAccount();
            manager.setNavigator(navigator);
            manager.setDataHandle(new GmailDataHandle(acct));
                    authenticator.loginWithGmail(acct.getIdToken(), new OnAuthenticateCallback() {
                        @Override
                        public void onSuccess(String UID ) {
                            manager.onSocialSuccess(UID);
                        }

                        @Override
                        public void onFailed(String errorMessage) {

                            manager.onFail(errorMessage);
                        }
                    });
        } else {

            // Signed out, show unauthenticated UI.
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
