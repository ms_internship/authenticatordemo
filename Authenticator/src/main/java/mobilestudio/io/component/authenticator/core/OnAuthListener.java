package mobilestudio.io.component.authenticator.core;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by pisoo on 9/18/2017.
 */

public interface OnAuthListener {
    void onSuccess (User user);
    void onFailed (String message );
}
