package mobilestudio.io.component.authenticator.core.datahandle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.io.Serializable;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by Sayed on 9/12/2017.
 */

public class Navigator implements Serializable {

    private Activity login;
    private Class register;
    private IDataHandle dataHandle;
    private Bundle bundle;

    public Navigator(Activity login, Class<? extends Activity> register) {
        this.login = login;
        this.register = register;
    }

    public void navigate(String UID) throws InterruptedException {
        User user = dataHandle.getUserData();
        user.setUid(UID);
        if (bundle == null)
            bundle = new Bundle();
        bundle.putSerializable("user", user);
        Intent intent = new Intent(login, register);
        intent.putExtra("bundle", bundle);
        intent.putExtra("isSocial", true);
        login.startActivity(intent);
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    public void setDataHandler(IDataHandle dataHandle) {
        this.dataHandle = dataHandle;
    }


}
