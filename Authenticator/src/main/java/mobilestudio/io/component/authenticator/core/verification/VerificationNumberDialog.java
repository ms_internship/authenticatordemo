package mobilestudio.io.component.authenticator.core.verification;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import mobilestudio.io.component.authenticator.R;
import mobilestudio.io.component.authenticator.core.authenticate.*;
import mobilestudio.io.component.authenticator.core.login.PhoneNumberLoginCallBack;

/**
 * Created by pisoo on 9/12/2017.
 */

public class VerificationNumberDialog extends DialogFragment implements View.OnClickListener, PhoneNumberLoginCallBack {
    private Button continuee, cancel;
    private EditText code;
    private TextView resendCode;
    private String mVerificationId;
    private String phoneNumber;
    private Authenticator authenticator;
    private FirebasePhoneNumberVerifier verifier;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.phone_number_dialog, container, false);
        if (getArguments() != null) {
            mVerificationId = getArguments().getString("VerificationId");
            phoneNumber = getArguments().getString("phoneNumber");
            verifier = (FirebasePhoneNumberVerifier) getArguments().getSerializable("verifier");
        }
        code = view.findViewById(R.id.ed_code);
        continuee = view.findViewById(R.id.bt_continue);
        cancel = view.findViewById(R.id.bt_cancel);
        resendCode = view.findViewById(R.id.tv_resendCode);
        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifier.resendCode();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VerificationNumberDialog.this.dismiss();
            }
        });
        continuee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyPhoneNumberWithCode(mVerificationId, code.getText().toString());
            }
        });
        getDialog().setCanceledOnTouchOutside(false);
        return view;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        verifier.sendCode(credential);
        this.dismiss();
    }

    @Override
    public void OnAddSuccessfully() {
        VerificationNumberDialog.this.dismiss();
    }

    @Override
    public void onAddWithFailure() {
        code.setError("The code number is wrong ");
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.bt_continue) {
            verifyPhoneNumberWithCode(mVerificationId, code.getText().toString());
        } else if (id == R.id.bt_cancel) {
            this.dismiss();
        } else if (id == R.id.tv_resendCode) {
            verifier.resendCode();
        }
    }

}
