package mobilestudio.io.component.authenticator.core.signup;

import android.widget.Button;
import android.widget.EditText;

import mobilestudio.io.component.authenticator.core.AuthenticationManager;
import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;
import mobilestudio.io.component.authenticator.core.authenticate.OnAuthenticateCallback;
import mobilestudio.io.component.authenticator.core.validator.DefaultEmailAndPasswordValidator;
import mobilestudio.io.component.authenticator.core.validator.EmailAndPasswordValdiator;

/**
 * Created by pisoo on 9/8/2017.
 */

public class EmailAndPasswordSignUpAuth extends BaseSignUpAuth {
    private EditText email, password, confirmPassword;
    private Button signup;
    private EmailAndPasswordValdiator valdiator = new DefaultEmailAndPasswordValidator();
    private AuthenticationManager manager;

    public EmailAndPasswordSignUpAuth(Authenticator authenticator, AuthenticationManager manager, EditText email, EditText password, EditText confirmPassword, final Button signup) {
        super(authenticator);
        this.manager = manager;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.signup = signup;
    }

    @Override
    public void signUp(final onSignUpCallback callback) {
        String emailString = email.getText().toString();
        String passwordString = password.getText().toString();
        if (valdiator.validateEmail(emailString) && valdiator.validatePassword(passwordString)) {
            authenticator.createUserWithEmailandPassword(emailString, passwordString, new OnAuthenticateCallback() {
                @Override
                public void onSuccess(String UID) {
                    if (callback != null)
                        callback.onSuccess();
                    manager.onSuccessNormal(UID);
                }

                @Override
                public void onFailed(String errorMessage) {
                    manager.onFail(errorMessage);
                }
            });
        } else if (!valdiator.validateEmail(emailString) && !valdiator.validatePassword(passwordString)) {
            manager.onFail("Wrong email address and password format");
        } else if (!valdiator.validatePassword(passwordString)) {
            manager.onFail("Wrong password format");
        } else if (!valdiator.validateEmail(emailString)) {
            manager.onFail("Wrong email address format");
        }
    }
}
