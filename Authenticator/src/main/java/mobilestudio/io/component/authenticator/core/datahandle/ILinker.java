package mobilestudio.io.component.authenticator.core.datahandle;

import com.google.firebase.auth.AuthCredential;

/**
 * Created by Sayed on 9/18/2017.
 */

public interface ILinker {
    void link(AuthCredential credential ,OnLinkCallback callback);
}
