package mobilestudio.io.component.authenticator.core.validator;

/**
 * Created by Sayed on 9/7/2017.
 */

public interface EmailAndPasswordValdiator {
    public Boolean validatePassword(String password);
    public Boolean validateEmail(String email);
}
