package mobilestudio.io.component.authenticator.core;

import mobilestudio.io.component.authenticator.core.datahandle.FirstTimeCallback;
import mobilestudio.io.component.authenticator.core.datahandle.IDBHandler;
import mobilestudio.io.component.authenticator.core.datahandle.IDataHandle;
import mobilestudio.io.component.authenticator.core.datahandle.Navigator;
import mobilestudio.io.component.authenticator.core.datahandle.OnDataFetched;
import mobilestudio.io.component.authenticator.core.datahandle.UserDataProvider;
import mobilestudio.io.component.authenticator.ui.model.User;


/**
 * Created by pisoo on 9/17/2017.
 */

public class AuthenticationManager {
    private static AuthenticationManager manager;

    private OnAuthListener listener;
    private boolean onSocial;
    private Navigator navigator;
    private IDBHandler databaseHandler;

    public void setDataHandle(IDataHandle dataHandle) {
        this.dataHandle = dataHandle;
        navigator.setDataHandler(dataHandle);
    }

    private IDataHandle dataHandle ;
    private UserDataProvider dataProvider;

    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

    public void setListener(OnAuthListener listener) {
        this.listener = listener;
    }

    public void setOnSocial(boolean onSocial) {
        this.onSocial = onSocial;
    }

    private AuthenticationManager(UserDataProvider dataProvider, IDBHandler handler) {
        this.databaseHandler = handler;
        this.dataProvider = dataProvider;
    }

    public static AuthenticationManager getInstance(UserDataProvider provider, IDBHandler dbHandler) {
        if (manager == null) {
            manager = new AuthenticationManager(provider, dbHandler);
        }
        return manager;
    }

    public void onSocialSuccess(final String UID) {
        databaseHandler.checkFirstTime(UID, new FirstTimeCallback() {
            @Override
            public void isFirstTime(boolean firstTime) throws InterruptedException {
                if (firstTime && onSocial) {
                    // navigate to the Activity and send the User data
                    navigator.navigate(UID);
                }else if ( !(onSocial)) {
                    listener.onSuccess(dataHandle.getUserData());
                }
                else
                {
                    dataProvider.loadUserFromDB(UID, new OnDataFetched() {
                        @Override
                        public void onSuccess(User user) {
                            listener.onSuccess(user);
                        }
                    });
                }
            }
        });

    }
    public void onSuccessNormal(String UID ) {
        User user = dataProvider.getUserLocal();
        user.setUid(UID);
        listener.onSuccess(user);
        databaseHandler.setUser(user);
    }

    public void onFail(String errorMessage) {
        listener.onFailed(errorMessage);
    }
}
