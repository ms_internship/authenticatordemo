package mobilestudio.io.component.authenticator.core.datahandle;

import mobilestudio.io.component.authenticator.ui.model.User;

/**
 * Created by pisoo on 9/15/2017.
 */

public class PhoneNumberDataHandle implements IDataHandle {
    String phone ;

    public PhoneNumberDataHandle(String phone) {
        this.phone = phone;
    }

    @Override
    public User getUserData() {
        User user = new User();
        user.setPhone(phone);
        return user;
    }
}
