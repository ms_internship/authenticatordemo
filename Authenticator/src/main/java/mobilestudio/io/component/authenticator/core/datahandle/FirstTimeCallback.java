package mobilestudio.io.component.authenticator.core.datahandle;

/**
 * Created by Sayed on 9/14/2017.
 */

public interface FirstTimeCallback {
    void isFirstTime(boolean firstTime) throws InterruptedException;
}
