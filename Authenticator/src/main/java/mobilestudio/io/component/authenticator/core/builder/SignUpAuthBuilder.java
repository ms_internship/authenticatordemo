package mobilestudio.io.component.authenticator.core.builder;

import android.app.Activity;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import mobilestudio.io.component.authenticator.core.AuthenticationManager;
import mobilestudio.io.component.authenticator.core.OnAuthListener;
import mobilestudio.io.component.authenticator.core.authenticate.Authenticator;
import mobilestudio.io.component.authenticator.core.datahandle.IDBHandler;
import mobilestudio.io.component.authenticator.core.datahandle.ILinker;
import mobilestudio.io.component.authenticator.core.datahandle.UserDataProvider;
import mobilestudio.io.component.authenticator.ui.model.User;
import mobilestudio.io.component.authenticator.core.signup.BaseSignUpAuth;
import mobilestudio.io.component.authenticator.core.signup.EmailAndPasswordSignUpAuth;
import mobilestudio.io.component.authenticator.core.signup.PhoneNumberSignUpAuth;
import mobilestudio.io.component.authenticator.core.signup.RegisterView;
import mobilestudio.io.component.authenticator.core.verification.Verifier;
import mobilestudio.io.component.authenticator.ui.view.DefaultSignUpView;
import mobilestudio.io.component.authenticator.ui.view.RegistrationViews;

/**
 * Created by pisoo on 9/8/2017.
 */

public class SignUpAuthBuilder {
    private BaseSignUpAuth EmailAndPasswordAuth , phoneNumberAuth;
    private Authenticator authenticator;
    private IDBHandler dbHandler ;
    private DefaultSignUpView signUpView;
    private User user;
    private Context context;
    private boolean onSocial ;

    public BaseSignUpAuth getEmailAndPasswordAuth() {
        return EmailAndPasswordAuth;
    }

    public BaseSignUpAuth getPhoneNumberAuth() {
        return phoneNumberAuth;
    }

    private RegistrationViews registrationViews = new RegistrationViews();
    private AuthenticationManager manager ;
    private ILinker linker ;
    private Verifier verifier ;
    private UserDataProvider userDataProvider ;


    public Context getContext() {
        return context;
    }


    public User getUser() {
        return user;
    }

    public AuthenticationManager getManager() {
        return manager;
    }

    public SignUpAuthBuilder(Authenticator authenticator , IDBHandler dbHandler  , Verifier verifier, OnAuthListener authListener) {
        this.authenticator = authenticator;
        userDataProvider = UserDataProvider.getInstance(dbHandler);
        userDataProvider.setRegistrationViews(registrationViews);
        manager = AuthenticationManager.getInstance(userDataProvider,dbHandler);
        manager.setListener(authListener);
        this.verifier = verifier ;
    }

    public SignUpAuthBuilder enableEmailAndPasswordView(EditText email, EditText password, EditText confirmPassword, Button signUp, boolean isRequired) {
        EmailAndPasswordAuth = new EmailAndPasswordSignUpAuth(authenticator,manager,email, password, confirmPassword, signUp);
        registrationViews.email = new RegisterView<>();
        registrationViews.email.setView(email, isRequired);
        registrationViews.password = new RegisterView<>();
        registrationViews.password.setView(password, isRequired);
        registrationViews.confirmPassword = new RegisterView<>();
        registrationViews.confirmPassword.setView(confirmPassword, isRequired);
        registrationViews.signup = new RegisterView<>();
        registrationViews.signup.setView(signUp, isRequired);
        return this;
    }

    public SignUpAuthBuilder enableDefaultEmailAndPassword(boolean isRequired) {
        signUpView.setEmailVisible();
        signUpView.setPasswordVisible();
        signUpView.setConfrimasswordVisible();
        signUpView.setSignUVisible();
        return enableEmailAndPasswordView(signUpView.getEmail(), signUpView.getPassword(), signUpView.getConfirmPassword(), signUpView.getSignup(), isRequired);
    }

    public SignUpAuthBuilder defaultView(Activity context, int id) {
        signUpView = context.findViewById(id);
        return this;
    }

    public boolean isOnSocial() {
        return onSocial;
    }

    public SignUpAuthBuilder setUserDataAtCompleteProfile(User user, boolean onSocial) {
        this.user = user;
        this.onSocial = onSocial;
        return this;
    }

    public SignUpAuthBuilder enablePhoneView(EditText phone, boolean isRequired , boolean isAuthenticated) {

         registrationViews.phone = new RegisterView<>();
         registrationViews.phone.setView(phone,(isAuthenticated || isRequired));
        if(isAuthenticated){
                phoneNumberAuth = new PhoneNumberSignUpAuth(authenticator,manager,verifier,registrationViews.phone.view);
            }


        return this;
    }

    public SignUpAuthBuilder enableDefaultPhoneView(boolean isRequired, boolean isAuthenticated) {
        signUpView.setPhoneVisible();
        return enablePhoneView(signUpView.getPhone(), isRequired,isAuthenticated);
    }

    public SignUpAuthBuilder enableNameView(EditText firstName, boolean isRequired) {
        registrationViews.firstName = new RegisterView<>();
        registrationViews.firstName.setView(firstName, isRequired);
        return this;
    }

    public SignUpAuthBuilder enableDefaultNameView(boolean isRequired) {
        signUpView.setFirstNameVisible();
        return enableNameView(signUpView.getFirstName(), isRequired);
    }

    public SignUpAuthBuilder enableFirstAndLastNameView(EditText firstName, EditText lastName, boolean isRequired) {
        registrationViews.firstName = new RegisterView<>();
        registrationViews.firstName.setView(firstName, isRequired);
        registrationViews.lastName = new RegisterView<>();
        registrationViews.lastName.setView(lastName, isRequired);
        return this;
    }

    public SignUpAuthBuilder enableDefaultFirstAndLastName(boolean isRequired) {
        signUpView.setFirstNameVisible();
        signUpView.setLastNameVisible();
        return enableFirstAndLastNameView(signUpView.getFirstName(), signUpView.getLastName(), isRequired);
    }

    public SignUpAuthBuilder enableBirthDateView(EditText birthDate, boolean isRequired, Context context) {
        this.context = context;
        registrationViews.birthdate.setView(birthDate, isRequired);
        return this;
    }

    public SignUpAuthBuilder enableDefaultBirthDateView(boolean isRequired, Context context) {

        signUpView.setBirthdateVisible();
        return enableBirthDateView(signUpView.getBirthdate(), isRequired, context);
    }

    public SignUpAuthController build() {
        return new SignUpAuthController(this);
    }

    public SignUpAuthBuilder enableDefaultGenderView(boolean isRequired) {
        signUpView.setGenderVisible();
        return enableGenderView(signUpView.getGender(), isRequired);
    }

    public SignUpAuthBuilder enableGenderView(Spinner gender, boolean isRequired) {
        registrationViews.gender.setView(gender, isRequired);
        return this;
    }

    public DefaultSignUpView getSignUpView() {
        return signUpView;
    }

    public RegistrationViews getRegistrationViews() {
        return registrationViews;
    }

    public SignUpAuthBuilder setLinker(ILinker linker){
        this.linker = linker;
        return  this;
    }

    public ILinker getLinker() {
        return linker;
    }
}
