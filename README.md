
### What is this repository for? ###

This Repo is a simple demo for Authentication Subsytem. Email and password auth , Phone verfication , Facebook Login and Gmail login. all features are customizable as your needs 

### How do I get set up? ###

till now you can get the module and import it manually in your code from File->New->Import Module then select the module from the demo.


### How To Use###
* Views 
### Default Login View 
		<mobilestudio.io.component.authenticator.ui.view.DefaultLoginView
           android:layout_width="match_parent"
           android:layout_height="match_parent"
           android:id="@+id/defaultLoginView"/>
### Default Siginup View
	<mobilestudio.io.component.authenticator.ui.view.DefaultSignUpView
   	   	  android:layout_height="match_parent"
          android:layout_width="match_parent"
    	  android:id="@+id/defaultSiginupView" />
		 
* Quick Call 
### Signin controller build

           LoginAuthController controller = new LoginAuthBuilder(new FirebaseAuthenticator(), // set the implmented auth, we have firebase one ready for usage
                                new FirebaseDBHandler("UserProfile"), // add the handler for the data with the endpoint to add profiles to
                                this, //  listener 
                                this)//  Activity
                .enableCompleteProfile( SignUpDefaultActivity.class)// the register activity for the complete profle " Optionally"
                .setBundle(bundle)   // you also have the option to add bundle to be transferred bwtween activites
                .defaultView( R.id.defaultView) // the id for the included default view
                .enableDefaultEmailAndPassword() // to enable the email and password login
                .enableDefaultGmail(gmailClientID) // enable gmail .. add the web application client id from google api console
                .enableDefaultPhone() // add phone login
                .enableDefaultFacebook() // add the facebook
                .build();
### Signup controller build

          SignUpAuthController controller = new SignUpAuthBuilder(new FirebaseAuthenticator(),
                         new FirebaseDBHandler("UsersProfile"),
                         new FirebasePhoneNumberVerifier(this),
                         this) // listener 
                .defaultView(this, R.id.defaultView)
                .enableDefaultEmailAndPassword(true)
                .enableDefaultBirthDateView(true, this)
                .enableDefaultFirstAndLastName(false)
                .setUserDataAtCompleteProfile(user, getIntent().getBooleanExtra("isSocial", false))
                .build();
                
* Listener 
You have to implment OnAuthListener which returns with 2 functions

         @Override
             public void onSuccess(User user) {
               // the registered user saved to the endpoint returned to you
             }
        @Override
             public void onFailed(String errorMessage) {
                // when the auth failed. the errormessage returned.
             }
## NOTES

* Don't forget to add the meta data of your facebook app into manifest of the app

* Don't forget to add the json file of your firebase app , also the gmailclient is the client of the webApp fron the api console


* Don't Forget to delegate the onActivityResult from the activity

         @Override
           protected void onActivityResult(int requestCode, int resultCode, Intent data) {
                 super.onActivityResult(requestCode, resultCode, data);
                 controller.onActivityResult(requestCode, resultCode, data);
             }
          
             
### Who do I talk to? ###

# Mustafa Gamal 
# Sayed El-Abady